# DoTo

Simple todo manager, whose main objective is to allow me to slowly get
used to ocaml build systems, practices, libraries, ...

It's not unique, but does what I need. The todo list managers that I've
seen where all doing too much or too little and require too much time
to be understood and used properly.

The only features of `doto` are save and use multiple todo lists, edit
the elements, change their status or priority, fork the list daily, backup
and cleanup the list. I am happy using it, despite its many rough edges,
but I hope they will slowly go away.

For the help use `doto --help`. Each command help can be reached with
`doto COMMAND --help`. Man pages and autocomplete are on the future
feature list for the moment.

# Credits

  - `ocaml-linebreak` library, courtesy of Christian Lindig. Original
  sourcecode in literate programming style is available on
  [github](https://github.com/lindig/ocaml-linebreak).

