open Containers

(** common cli options *)
type copts = { compact: bool; todo_id: string option; }

(** cli commands implementation *)

let todo_list ~todos id =
  let open Result in
  let open Todos in
  let len = String.length id in
  let todo' =
    List.filter (fun tl ->
      String.is_sub 0 tl._id 0 ~sub:id ~len
    ) todos
  in
  match todo' with
  | [todo] -> Ok(todo)
  | [] -> Error (Format.sprintf "No todo list with uuid %s." id)
  | _  -> Error (Format.sprintf "Multiple todo lists match uuid %s." id)

let print_todos todos copts =
  let open Result in
  let printer = Todos.print ~show_priority:true ~compact:copts.compact ~shift:2 in
  begin
    match copts.todo_id with
    | Some id ->
      begin
        match todo_list ~todos id with
        | Ok(tl) -> printer tl
        | Error(e) -> failwith ("Error: "^e)
      end
    | None -> List.iter printer todos
  end

let do_thing_with copts todos thing =
  let todos' =
    match copts.todo_id with
    | Some id ->
      begin
        match todo_list ~todos id with
        | Ok(tl) -> thing tl
        | Error(e) -> failwith ("Error: "^e)
      end
    | None -> if (List.length todos) = 1
      then thing (List.hd todos)
      else failwith "Error: No todo list present. \
                     Run `doto --help` for additional informations."
  in print_todos todos' copts

let get_item tl item_id =
  let open Todos in
  match List.filter (fun i ->
    String.is_sub 0 i.Item._id 0 ~sub:item_id ~len:(String.length item_id)
  ) tl.items with
  | [item] -> item
  | [] -> failwith (Format.sprintf "Error: Unable to find the item in \"%s\"." tl.title)
  | _ -> failwith (Format.sprintf "Error: Multiple items match uuid %s in \"%s\"." item_id tl.title)

let add_item' config todos tl item =
  let open Todos in
  let todos' = List.remove ~eq:(fun t t' -> t._id = t'._id) ~x:tl todos in
  let todos'' = {tl with items = Item.sorted (item::tl.items)} :: todos' in
  Serialize.save config todos''; todos''

let rm_item' config todos tl item_id =
  let open Todos in
  let todos' = List.remove ~eq:(fun t t' -> t.Todos._id = t'.Todos._id) ~x:tl todos in
  let item = get_item tl item_id in
  let items' = List.remove ~eq:(fun i i' -> i.Item._id = i'.Item._id) ~x:item tl.items in
  let todos'' = {tl with items = Item.sorted items'} :: todos' in
  Serialize.save config todos''; todos''

let add_comment' config todos tl item_id comment =
  let open Todos in
  let todos' = List.remove ~eq:(fun t t' -> t.Todos._id = t'.Todos._id) ~x:tl todos in
  let item = get_item tl item_id in
  let items' = List.remove ~eq:(fun i i' -> i.Item._id = i'.Item._id) ~x:item tl.items in
  let item' = {item with Item.comments= comment::item.Item.comments} in
  let todos'' = {tl with items = Item.sorted (item'::items')} :: todos' in
  Serialize.save config todos''; todos''

let edit_title' config todos tl item_id title =
  let open Todos in
  let todos' = List.remove ~eq:(fun t t' -> t.Todos._id = t'.Todos._id) ~x:tl todos in
  let item = get_item tl item_id in
  let items' = List.remove ~eq:(fun i i' -> i.Item._id = i'.Item._id) ~x:item tl.items in
  let item' = {item with Item.title=title} in
  let todos'' = {tl with items = Item.sorted (item'::items')} :: todos' in
  Serialize.save config todos''; todos''

let edit_status' config todos tl item_id status =
  let open Todos in
  let todos' = List.remove ~eq:(fun t t' -> t.Todos._id = t'.Todos._id) ~x:tl todos in
  let item = get_item tl item_id in
  let items' = List.remove ~eq:(fun i i' -> i.Item._id = i'.Item._id) ~x:item tl.items in
  let item' = {item with Item.status=status} in
  let todos'' = {tl with items = Item.sorted (item'::items')} :: todos' in
  Serialize.save config todos''; todos''

let edit_priority' config todos tl item_id priority =
  let open Todos in
  let todos' = List.remove ~eq:(fun t t' -> t.Todos._id = t'.Todos._id) ~x:tl todos in
  let item = get_item tl item_id in
  let items' = List.remove ~eq:(fun i i' -> i.Item._id = i'.Item._id) ~x:item tl.items in
  let item' = {item with Item.priority=priority} in
  let todos'' = {tl with items = Item.sorted (item'::items')} :: todos' in
  Serialize.save config todos''; todos''

let print_list todos _ =
  let open Todos in
  print_endline "Available todo lists:";
  List.iter (fun tl ->
    let chopped_id = String.sub tl._id 0 5 in
    let msg = Format.sprintf "  - {%s} @{<White>%s@}"
                chopped_id tl.title
    in
    print_endline msg
  ) todos

let add_todo config todos copts title =
  let todos' = (Todos.make title [])::todos in
  Serialize.save config todos';
  print_todos todos copts

let backup_todo config todos _copts =
  Serialize.save ~is_backup:true config todos;
  print_endline "Backup correctly saved."

let cleanup_todo config (todos:Todos.todo_lists) copts =
  let open Todos in
  let cleanup tl =
    let items' = List.filter (fun it ->
      match it.Item.status with
      | `Done _ -> false
      | _ -> true
    ) tl.items in
    (* items are already sorted, no need to sort again *)
    {tl with items=items'}
  in
  let thing tl =
    let tl' = cleanup tl in
    let todos' = List.remove ~eq:(fun t t' -> t._id = t'._id) ~x:tl todos in
    let todos'' = tl'::todos' in
    begin
      Serialize.save config todos''; todos''
    end
  in
  (* always make a backup on cleanup *)
  Serialize.save ~is_backup:true config todos;
  match copts.todo_id with
  | Some _ -> do_thing_with copts todos thing
  | None ->
    let todos' = List.map cleanup todos in
    begin
      Serialize.save config todos';
      print_todos todos' copts
    end

let rm_todo config todos copts id =
  match todo_list ~todos id with
  | Ok(tl) -> begin
      let todos' = List.remove ~eq:(fun t t' -> t.Todos._id = t'.Todos._id) ~x:tl todos in
      Serialize.save config todos';
      print_todos todos' copts
    end
  | Error(e) -> failwith ("Error: "^e)

let add_item config todos copts title priority status comments =
  let item = Item.make title priority status comments in
  let thing tl = add_item' config todos tl item in
  do_thing_with copts todos thing

let rm_item config todos copts item_id =
  let thing tl = rm_item' config todos tl item_id in
  do_thing_with copts todos thing

let add_comment config todos copts item_id comment =
  let thing tl = add_comment' config todos tl item_id comment in
  do_thing_with copts todos thing

let edit_title config todos copts item_id title =
  let thing tl = edit_title' config todos tl item_id title in
  do_thing_with copts todos thing

let edit_status config todos copts item_id status =
  let thing tl = edit_status' config todos tl item_id status in
  do_thing_with copts todos thing

let edit_priority config todos copts item_id priority =
  let thing tl = edit_priority' config todos tl item_id priority in
  do_thing_with copts todos thing