open Utils

exception DoTo_Config_exn of string

type doto_config = {
  data_store: string  (* Unix.getenv "HOME" *)
}

(** TODO: implement custom .doto/config config file *)
let load_config () =
  let data_store =
    try
      Unix.getenv "DOTO_STORE"
    with Not_found ->
      Format.sprintf "%s/.doto/store" (Unix.getenv "HOME")
  in
  let folder_exists =
    try
      let s = Unix.stat data_store in
      s.st_kind = Unix.S_DIR || s.st_kind = Unix.S_LNK
    with Unix.Unix_error (Unix.ENOENT, _, _) -> false
  in
  if not folder_exists then begin
    let msg = Printf.sprintf
                "-> Doto folder (%s) does not exist.\
                \ I will try to create it."
                data_store
    in
    print_endline msg;
    mkdir_p data_store;
    print_endline "   Done!\n"
  end;
  {data_store;}
