open Containers

(** item priority *)
type priority_t = [
    `High
  | `Normal
  | `Low
][@@deriving yojson]

(** item status *)
type status_t = [
    `Done of float (* Unix timestamp *)
  | `Blocked of float (* Unix timestamp *)
  | `InProgress
  | `Paused
  | `ToDo
][@@deriving yojson]

(** todo list item *)
type item = {
  _id: string; (* Uuid *)
  created: float; (* Unix timestamp *)
  modified: float; (* Unix timestamp *)
  title: string;
  priority: priority_t;
  status: status_t;
  comments: string list;
} [@@deriving yojson]

(** new item *)
let make title priority status comments =
  let _id = Utils.get_uuid_v4 () in
  let created = Unix.gettimeofday () in
  {_id; created; modified=created; title; priority; status; comments;}

(** append comments to an existing item *)
let add_comments item comments' =
  let comments = List.append item.comments comments' in
  let modified = Unix.gettimeofday () in
  {item with comments; modified}

(** format the timestamp into a readable date *)
let format_date timestamp =
  let open CalendarLib in
  timestamp
  |> Calendar.from_unixfloat
  |> Printer.Calendar.sprint "%a %d %b '%y" (* (%a, %d %b %y %H:%M:%S) *)

let sorted items =
  let item_value item =
    let status_c =
      match item.status with
      | `Done _ -> 5
      | `InProgress -> 4
      | `Paused -> 3
      | `Blocked _ -> 2
      | `ToDo -> 1
    in
    let priority_c =
      match item.priority with
      | `High -> 20
      | `Normal -> 10
      | `Low -> 0
    in
    status_c + priority_c
  in
  List.sort (fun i i' -> compare (item_value i) (item_value i')) items

(** item pretty printer *)
let print ?(compact=true) ?(shift=4) item =
  let open Format in
  (*set_color_default true;*)
  let line_len =
    match Terminal_size.get_columns () with
    | Some size -> size
    | None -> 80
  in
  let indent = if shift>0 then String.repeat " " shift else "" in
  let id_s = String.sub item._id 0 5 in
  let uid = sprintf "{%s}" id_s in
  let date = if compact then ""
    else
      match item.status with
      | `Done date | `Blocked date -> sprintf "(%s)" (format_date date)
      | _ -> ""
  in
  let status = match item.status with
    | `Done _ -> sprintf "@{<green>[✔︎]@}";
    | `InProgress -> sprintf "@{<yellow>[ ]@}";
    | `Paused -> sprintf "@{<yellow>[•]@}";
    | `Blocked _ -> sprintf "@{<Red>[✘]@}";
    | `ToDo -> sprintf "@{<white>[ ]@}";
  in
  let title = sprintf "@{<white>%s@}" item.title in
  let footer =
    let c_no = List.length (item.comments) in
    if compact && c_no > 0 then
      sprintf "(%d comment%s)" c_no (if c_no = 1 then "" else "s")
    else ""
  in
  let splitlines ?(s=0) l = Linebreak.Smart.break
                       (line_len -shift -4 -s)
                       (String.Split.list_cpy ~by:" " l)
  in
  let headline = String.concat " " [indent; status;] in
  let headline' = String.concat " " [indent; "   ";] in
  let restline = String.concat " " [title; footer; uid;] in
  let restlines = splitlines restline in
  List.iteri (fun i ws ->
    let prefix = if i = 0 then headline else headline'
    in
    print_endline @@ sprintf "%s %s" prefix (String.concat " " ws)
  ) restlines;
  if date <> "" then
    print_endline (sprintf "%s     %s" indent date);
  if not compact then
    let sep i = if i = 0 then "-" else " " in
    let cmts = List.map (fun c -> splitlines ~s:2 c) item.comments in
    List.iter (fun cmt ->
      List.iteri (fun i ws ->
        print_endline @@ sprintf "%s %s %s" headline' (sep i) (String.concat " " ws)
      ) cmt
    ) cmts;
