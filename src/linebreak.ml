
# 204 "format.lp"

# 173 "format.lp"
(* This code is extracted from a literate program at 
   http://github.com/lindig/ocaml-linebreak. Please don't
   modify this code directly but the literate program instead.

   (c) Christian Lindig 2012, 2013 <lindig@gmail.com>
*)


# 204 "format.lp"


# 260 "format.lp"
let (@@) f x    = f x


# 266 "format.lp"
type words  = string list 
type lines  = words list

let length = String.length
let spacewidth = 1 (* width of a space *)
let square (x:int): int = x*x

# 205 "format.lp"

module Simple = struct
    
# 607 "format.lp"
let break (width:int) (words:words): lines =
    let len   = String.length in
    let rev   = List.rev in
    let rec loop w lines words = match lines, words with
        | line::lines, word::words when len word <= w -> 
            loop (w - len word - spacewidth) ((word::line)::lines) words
        | line::lines, word::words  -> 
            loop (width - len word - spacewidth) 
                    ([word] :: rev line :: lines) words
        | line::lines,[]  -> rev (rev line :: lines)
        | [], words -> loop w [[]] words
    in
        loop width [] words

# 207 "format.lp"

end    
module Smart = struct
    
# 498 "format.lp"
type cost       =   int
type break      =   string * cost * int
type par        =   break list


# 503 "format.lp"
let dump (par:par):unit =
    let break (word, cost, skip) = 
        Printf.printf "%4d %+2d %2d %s\n" cost skip (length word) word
    in
        List.iter break @@ List.rev par


# 513 "format.lp"
let minimum: par -> cost = function
    | []            -> 0
    | (_,cost,_)::_ -> cost


# 533 "format.lp"
let rec add' (word:string) (avail:int) (n:int) cost skip par =
    (* let spacewidth = 1 in  debug *)
    let need w = length w + spacewidth in (* need room for word + space *)
    match par with
    | []                             -> word, cost, skip (* best so far *)
    | (w,c,s)::par when need w > avail -> word, cost, skip (* best so far *)
    | (w,c,s)::par ->
        let cost'   = square (avail - need w) + minimum par in
        let skip'   = n+1 in
        let avail'  = avail - need w in
            if cost' < cost (* better breakpoint found? *)
            then add' word avail' skip' cost' skip' par 
            else add' word avail' skip' cost  skip  par 


# 555 "format.lp"
let add_word (word:string) (linewidth:int) (par:par): break = 
    let avail   = linewidth - length word in
    let cost    = square avail + minimum par in
        add' word avail 0 cost 0 par 


# 566 "format.lp"
let doc (par:par): lines =
    let rec loop n par words lines = match n, par with
        | _, []         -> words::lines
        | 0, (w,c,n)::p -> loop n p [w] (words::lines)
        | n, (w,c,_)::p -> loop (n-1) p (w::words) lines
    in match par with
    | []         -> []
    | (w,c,n)::p -> loop n p [w] []

# 581 "format.lp"
let break' (linewidth:int) (words:words): par = 
    let rec loop paragraph = function
    | []            -> paragraph
    | word :: words -> 
        loop (add_word word linewidth paragraph :: paragraph) words
    in
        loop [] words


# 590 "format.lp"
let break linewidth words = doc @@ break' linewidth words
let debug linewidth words = dump @@ break' linewidth words

# 210 "format.lp"

end 
