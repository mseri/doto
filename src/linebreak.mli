
# 181 "format.lp"

# 173 "format.lp"
(* This code is extracted from a literate program at 
   http://github.com/lindig/ocaml-linebreak. Please don't
   modify this code directly but the literate program instead.

   (c) Christian Lindig 2012, 2013 <lindig@gmail.com>
*)


# 181 "format.lp"

type words  = string list 
type lines  = words list

val spacewidth: int (* 1 *)

module Simple: sig
    val break: int -> words -> lines
    (** [break n words] breaks words into lines of width w *)
end

module Smart: sig
    val break: int -> words -> lines
    (** [break n words] breaks words into lines of width w *)

    val debug: int -> words -> unit
    (** [debug n words] dumps the internal representation of 
    the paragraph to stdout. This can be helpful for debugging
    or understanding the algorithm. *)
end
