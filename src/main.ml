open Containers
open Cmdliner
open Cli_ops

(* Help sections common to all commands *)
let copts_sect = "COMMON OPTIONS"

let copts compact todo_id  = { compact; todo_id }
let copts_t =
  let docs = copts_sect in
  let compact =
    let doc = "Use compact output style." in
    Arg.(value & flag & info ["c"; "compact"] ~docs ~doc)
  in
  let todo_id =
    let doc = "Select a specific todo list by passing its $(docv)."
    in
    Arg.(value & opt (some string) None & info ["t"; "todo"] ~docv:"TODO_LIST_ID" ~docs ~doc)
  in
  Term.(const copts $ compact $ todo_id)

(* Commands *)
let add_todo_cmd config todos =
  let title = Arg.(required & pos 0 (some string) None & info [] ~docv:"TITLE") in
  let doc = "Create a new todo list with title TITLE." in
  Term.(const (add_todo config todos) $ copts_t $ title),
  Term.info "todo" ~doc ~sdocs:copts_sect

let lists_cmd todos =
  let doc = "Print the list of available todo lists and their uuids." in
  Term.(const (print_list todos) $ copts_t),
  Term.info "todos-list" ~doc ~sdocs:copts_sect

let cleanup_todo_cmd config todos =
  let doc = "Garbage collect the Done items from the todo list \
             (it also creates a backup the old list)." in
  Term.(const (cleanup_todo config todos) $ copts_t),
  Term.info "todos-cleanup" ~doc ~sdocs:copts_sect

let backup_todo_cmd config todos =
  let doc = "Create a backup of the current todo lists." in
  Term.(const (backup_todo config todos) $ copts_t),
  Term.info "todos-backup" ~doc ~sdocs:copts_sect

let rm_todo_cmd config todos =
  let id = Arg.(required & pos 0 (some string) None & info [] ~docv:"UUID") in
  let doc = "Removes the todo list with id TODO_UUID" in
  Term.(const (rm_todo config todos) $ copts_t $ id),
  Term.info "todos-rm" ~doc ~sdocs:copts_sect

let add_item_cmd config todos =
  let comments =
    let doc = "Append $(docv) to the comments list of the todo" in
    Arg.(value & opt_all string [] & info ["c"; "comment"] ~docv:"COMMENT" ~doc)
  in
  let priority =
    let doc = "Priority of the todo. It can be low `l`, normal `n` (default) or high `h`" in
    let prio = Arg.enum ["l", `Low; "n", `Normal; "h", `High] in
    Arg.(value & opt prio `Normal & info ["p"; "priority"] ~docv:"PRIORITY" ~doc)
  in
  let status =
    let doc = "Status of the todo. It can be `todo`, `in-progress`, `puased`, `blocked`, or `done`" in
    let stat = Arg.enum [
      "todo", `ToDo;
      "paused", `Paused;
      "in-progress", `InProgress;
      "blocked", `Blocked (Unix.gettimeofday ());
      "done", `Done (Unix.gettimeofday ());
    ] in
    Arg.(value & opt stat `ToDo & info ["s"; "status"] ~docv:"STATUS" ~doc)
  in
  let title = Arg.(required & pos 0 (some string) None & info [] ~docv:"TITLE") in
  let doc = "Create a new item with title TITLE in the selected todo list." in
  Term.(const (add_item config todos) $ copts_t $ title $ priority $ status $ comments),
  Term.info "item" ~doc ~sdocs:copts_sect

let rm_item_cmd config todos =
  let id = Arg.(required & pos 0 (some string) None & info [] ~docv:"ITEM_UUID") in
  let doc = "Remove the item with ITEM_UUID from the selected todo list." in
  Term.(const (rm_item config todos) $ copts_t $ id),
  Term.info "item-rm" ~doc ~sdocs:copts_sect

let add_comment_cmd config todos =
  let id = Arg.(required & pos 0 (some string) None & info [] ~docv:"ITEM_UUID") in
  let comment = Arg.(required & pos 1 (some string) None & info [] ~docv:"COMMENT") in
  let doc = "Appent COMMENT to the comments of the item ITEM_UUID in the selected todo list." in
  Term.(const (add_comment config todos) $ copts_t $ id $ comment),
  Term.info "item-comment" ~doc ~sdocs:copts_sect

let edit_title_cmd config todos =
  let id = Arg.(required & pos 0 (some string) None & info [] ~docv:"ITEM_UUID") in
  let title = Arg.(required & pos 1 (some string) None & info [] ~docv:"TITLE") in
  let doc = "Changes the TITLE the item ITEM_UUID in the selected todo list." in
  Term.(const (edit_title config todos) $ copts_t $ id $ title),
  Term.info "item-title" ~doc ~sdocs:copts_sect

let edit_status_cmd config todos =
  let id = Arg.(required & pos 0 (some string) None & info [] ~docv:"ITEM_UUID") in
  let stat = Arg.enum [
    "todo", `ToDo;
    "paused", `Paused;
    "in-progress", `InProgress;
    "blocked", `Blocked (Unix.gettimeofday ());
    "done", `Done (Unix.gettimeofday ());
  ] in
  let status = Arg.(required & pos 1 (some stat) None & info [] ~docv:"STATUS") in
  let doc = "Changes the STATUS the item ITEM_UUID in the selected todo list." in
  Term.(const (edit_status config todos) $ copts_t $ id $ status),
  Term.info "item-status" ~doc ~sdocs:copts_sect

let edit_priority_cmd config todos =
  let id = Arg.(required & pos 0 (some string) None & info [] ~docv:"ITEM_UUID") in
  let prio = Arg.enum [
    "low", `Low;
    "normal", `Normal;
    "high", `High
  ] in
  let priority = Arg.(required & pos 1 (some prio) None & info [] ~docv:"PRIORITY") in
  let doc = "Changes the PRIORITY of the item with id ITEM_UUID in the selected todo list." in
  Term.(const (edit_priority config todos) $ copts_t $ id $ priority),
  Term.info "item-priority" ~doc ~sdocs:copts_sect

let default_cmd todos =
  let doc = "a simple todo lists control system" in
  let man = [
    `S "DESCRIPTION";
    `P "$(tname) allows to manage todo lists in a simple way. \
        It keeps the daily history of your lists and will ``fork'' \
        a new list for each day it's being used. In the future \
        it will be possible to access to old versions of the \
        lists. Use `$(tname) COMMAND --help` \
        for help on the command itself."
  ] in
  (* program help (+style) *)
  Term.(const (print_todos todos) $ copts_t),
  Term.info "doto" ~version:"0.2.1" ~sdocs:copts_sect ~doc ~man

let cmds ~config ~todos = [lists_cmd todos;
                           add_todo_cmd config todos;
                           rm_todo_cmd config todos;
                           cleanup_todo_cmd config todos;
                           backup_todo_cmd config todos;
                           add_item_cmd config todos;
                           rm_item_cmd config todos;
                           edit_title_cmd config todos;
                           add_comment_cmd config todos;
                           edit_status_cmd config todos;
                           edit_priority_cmd config todos
                          ]

let cli ~config ~todos =
  match Term.eval_choice (default_cmd todos) (cmds ~config ~todos) with
  | `Error _ -> exit 1 | _ -> exit 0

let () =
  Format.set_color_default true;
  let config = Config.load_config () in
  let todos = Serialize.load config in
  (* find a cleaner solution to clear screen... *)
  ignore(Sys.command "clear");
  cli ~config ~todos

