open Containers

(** item serialization and deserialization helpers *)
let serialize tls =
  tls |> Todos.todo_lists_to_yojson |> Yojson.Safe.to_string

let deserialize tls =
  tls |> Yojson.Safe.from_string |> Todos.todo_lists_of_yojson

let filename_from_timestamp ?(modifier="") timestamp =
  let open CalendarLib in
  timestamp
  |> Date.from_unixfloat
  |> Printer.Date.sprint "%Y%m%d"
  |> fun d ->
  if modifier <> ""
  then Format.sprintf "%s-%s.json" d modifier
  else Format.sprintf "%s.json" d

let date_from_filename filename =
  let open CalendarLib in
  filename
  |> Printer.Date.from_fstring "%Y%m%d.json"

let load (config: Config.doto_config) =
  let filelist = Array.filter (fun fn -> not (String.contains fn '-'))
                   (Sys.readdir config.data_store)
  in
  let f f1 f2 =
    let open CalendarLib in
    - Date.compare (date_from_filename f1) (date_from_filename f2)
  in
  Array.fast_sort f filelist;
  match Array.get_safe filelist 0 with
  | None -> []
  | Some name ->
    let filename = Format.sprintf "%s/%s" (config.data_store) name in
    match CCIO.(with_in filename read_all) |> String.trim |> deserialize with
    | Ok tls -> tls
    | Error e -> failwith (Format.sprintf "Error loading the todo list: %s" e)

let save (config: Config.doto_config) ?(is_backup=false) todo_lists =
  let data = serialize todo_lists in
  let now = Unix.gettimeofday () in
  let filename =
    (
      if is_backup
      (* XXX: consider replacing unix-timestamp with hash *)
      then (filename_from_timestamp ~modifier:(string_of_float now) now)
      else (filename_from_timestamp now)
    ) |> Format.sprintf "%s/%s" (config.data_store)
  in CCIO.(with_out filename (fun f -> write_line f data))
