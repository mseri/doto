open Containers
open Item

(** todo list *)
type todo_list = {
  _id: string;
  parent: string option;  (* It could be a new list or a clone *)
  created: float; (* Unix timestamp *)
  title: string;
  items: item list;
}[@@deriving yojson]

(** loaded lists *)
type todo_lists = todo_list list [@@deriving yojson]

(** new item *)
let make ?(parent) title items =
  let _id = Utils.get_uuid_v4 () in
  let created = Unix.gettimeofday () in
  {_id; created; parent; title; items;}

(** append comments to an existing item *)
let add_items todo_list items' =
  let items = List.append todo_list.items items' in
  {todo_list with items;}

(** return the todo list where the items have been sorted with f *)
let sort_by f todo_list =
  {todo_list with items = List.sort f (todo_list.items)}

(** todo_list with item sorted by creation time *)
let sort_by_creation_date ?(reverse=false) todo_list =
  let a = if reverse then 1 else -1 in
  let f (item1: Item.item) (item2: Item.item) =
    if item1.created < item2.created then -a
    else if item1.created > item2.created then a
    else 0
  in sort_by f todo_list

(** todo_list with item sorted by last edit time *)
let sort_by_edit_date ?(reverse=false) todo_list =
  let a = if reverse then 1 else -1 in
  let f (item1: Item.item) (item2: Item.item) =
    if item1.modified < item2.modified then -a
    else if item1.modified > item2.modified then a
    else 0
  in sort_by f todo_list

(** todo_list with item sorted by priority *)
let sort_by_priority ?(reverse=false) todo_list =
  let a = if reverse then 1 else -1 in
  let f (item1: Item.item) (item2: Item.item) =
    match (item1.priority, item2.priority) with
    | (a, b) when a=b -> 0 (* to avoid flipping items *)
    | (`High, _) | (_, `Low) -> a
    | (_, `High) | (`Low, _) -> -a
    | _ -> 0
  in sort_by f todo_list

let print ?(show_priority=false) ?(compact=true) ?(shift=4)
      {_id=_; parent=_; created=_; title; items} =
  let open Format in
  (*set_color_default true;*)
  let string_of_priority = function
    | `High -> "High"
    | `Normal -> "Normal"
    | `Low -> "Low"
  in
  let get_items priority = (
    priority,
    List.filter (fun i -> i.priority = priority) items
  ) in
  let grouped_items =
    List.filter (fun (_, l) -> List.length l > 0)
      [get_items `High; get_items `Normal; get_items `Low]
  in
  let formatted_title = Format.sprintf "@{<White>%s@}" title in
  print_endline (Utils.fill_row ~txt_len:(String.length title) formatted_title);
  List.iter (fun (priority, items) ->
    if show_priority
    then
      let priority = string_of_priority priority in
      let subtitle = Format.sprintf "@{<White>%s@}" priority in
      let formatted_subtitle = Utils.fill_row ~align:Utils.Left ~pattern:"~" ~txt_len:(String.length priority) subtitle in
      print_endline formatted_subtitle;
      List.iter (fun i -> Item.print ~compact ~shift i) items;
      print_newline ();
  ) grouped_items
