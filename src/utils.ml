open Containers

(** simulate mkdir -p.
 *  Note: raise exn if anything in path is not (linked to) a standard folder *)
let rec mkdir_p path =
  let is_or_links_directory path =
    try
      match (Unix.stat path).st_kind with
      | Unix.S_DIR -> true
      | Unix.S_LNK -> (Unix.lstat path).st_kind = Unix.S_DIR
      | _ -> raise (Unix.Unix_error (Unix.ENOTDIR, "stat", path))
    with Unix.Unix_error (Unix.ENOENT, _, _) -> false
  in
  (* if the path is void or already exist, we can safely finish *)
  if path = ""  || is_or_links_directory path
  then ()
  else begin
    (* check or create the parent if needed *)
    let parent = Filename.dirname path in
    if parent <> "." then mkdir_p parent;
    (* finally create the current folder *)
    Unix.mkdir path 0o777
  end

(** string representing a freshly generated UUIDv4 *)
let get_uuid_v4 () = Uuidm.(v `V4 |> to_string ~upper:false)

type alignment = Left | Center | Right

let fill_row ?(pattern) ?(align=Center) ?(txt_len) text =
  let pattern =
    match pattern with
    | None | Some "" -> "="
    | Some p -> p
  in
  let fill len =
    let text_length = match txt_len with
      | Some n -> n
      | None -> String.length text
    in
    let free_space = max 0 (len - text_length - 2) in
    let factor = String.length pattern in
    let length = free_space / factor in
    let content =
      match align with
      | Left | Right ->
        let length = max 0 (length - factor - 1) in
        let filler = String.repeat pattern length in
        if align = Left then [pattern; text; filler]
        else [filler; text; pattern]
      | Center ->
        let length = length / 2 in
        let filler = String.repeat pattern length in
        [filler; text; filler]
    in String.concat " " content
  in
  match Terminal_size.get_columns () with
  | Some cols -> fill cols
  | None -> fill 80
